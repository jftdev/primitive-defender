package fr.jftdev.jeux.primitivedefender.manager;

import java.util.HashMap;
import java.util.Map;

import fr.jftdev.jeux.primitivedefender.entity.PDEntity;
import fr.jftdev.jeux.primitivedefender.entity.PDEntityPool;
import fr.jftdev.jeux.primitivedefender.entity.PDEntityType;
import fr.jftdev.jeux.primitivedefender.entity.bullet.test.Bullet1Test;
import fr.jftdev.jeux.primitivedefender.entity.bullet.test.Bullet2Test;
import fr.jftdev.jeux.primitivedefender.entity.enemy.test.EnemyTest;
import fr.jftdev.jeux.primitivedefender.entity.enemy.test.EnemyTestGreen;

public class PoolManager {
	
	public Map<PDEntityType, PDEntityPool<? extends PDEntity>> pools;
	
	private static PoolManager INSTANCE = new PoolManager();
	
	private PoolManager() {
		pools = new HashMap<PDEntityType, PDEntityPool<? extends PDEntity>>();
		
		PDEntityPool<EnemyTest> poolNoir = new PDEntityPool<EnemyTest>(EnemyTest.class, 10);
		PDEntityPool<EnemyTestGreen> poolVert = new PDEntityPool<EnemyTestGreen>(EnemyTestGreen.class, 30);
		PDEntityPool<Bullet1Test> poolBullet1Test = new PDEntityPool<Bullet1Test>(Bullet1Test.class, 30);
		PDEntityPool<Bullet2Test> poolBullet2Test = new PDEntityPool<Bullet2Test>(Bullet2Test.class, 30);
		
		pools.put(PDEntityType.RECTANGLE_NOIR, poolNoir);
		pools.put(PDEntityType.RECTANGLE_VERT, poolVert);
		pools.put(PDEntityType.BULLET_1_TEST, poolBullet1Test);
		pools.put(PDEntityType.BULLET_2_TEST, poolBullet2Test);
	}
	
	public static PoolManager getInstance() {
		return INSTANCE;
	}
	
	public PDEntityPool<? extends PDEntity> getPool(PDEntityType type) {
		return pools.get(type);
	}
}
