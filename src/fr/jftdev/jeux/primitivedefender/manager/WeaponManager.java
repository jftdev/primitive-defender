package fr.jftdev.jeux.primitivedefender.manager;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;

import fr.jftdev.jeux.primitivedefender.entity.weapon.Weapon;
import fr.jftdev.jeux.primitivedefender.entity.weapon.test.Weapon1Test;
import fr.jftdev.jeux.primitivedefender.entity.weapon.test.Weapon2Test;
import fr.jftdev.jeux.primitivedefender.util.constant.WeaponConstants;

public class WeaponManager {
	
	protected Weapon weapon1;
	protected Weapon weapon2;
	protected Weapon weapon3;
	protected Weapon weapon4;
	protected Weapon weapon5;
	protected Weapon[] weapons = new Weapon[5];
	
	public WeaponManager(Scene scene, Entity enemiesLayer) {
		weapons[0] = new Weapon1Test(0.0f, 0.0f, enemiesLayer);
		weapons[1] = new Weapon2Test(0.0f, 0.0f, enemiesLayer);
		weapons[2] = new Weapon1Test(0.0f, 0.0f, enemiesLayer);
		weapons[3] = new Weapon2Test(0.0f, 0.0f, enemiesLayer);
		weapons[4] = new Weapon1Test(0.0f, 0.0f, enemiesLayer);
		scene.attachChild(weapons[0]);
		scene.attachChild(weapons[1]);
		scene.attachChild(weapons[2]);
		scene.attachChild(weapons[3]);
		scene.attachChild(weapons[4]);
	}
	
	public void fire(float pX, float pY, int weaponId) {
		for(int i=0; i<weapons.length; i++) {
			weapons[i].stop();
		}
		weapons[weaponId].fire(pX, pY);
	}
	
	public void stop(int weaponId) {
		weapons[weaponId].stop();
	}

}
