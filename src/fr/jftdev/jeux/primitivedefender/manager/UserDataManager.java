package fr.jftdev.jeux.primitivedefender.manager;

import android.content.Context;
import android.content.SharedPreferences;

public class UserDataManager {
	
	private static final String PREFS_NAME = "GAME_USERDATA";
	
	private static final String UNLOCKED_LEVEL_KEY = "unlockedLevels";
	private static final String SOUND_KEY = "soundKey";
	private static final String MUSIC_KEY = "musicKey";
	
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	
	private int unlockedLevels;
	private boolean soundEnabled;
	private boolean musicEnabled;
	
	private static UserDataManager INSTANCE = new UserDataManager();
	
	private UserDataManager() {
		
	}
	
	public static UserDataManager getInstance() {
		return INSTANCE;
	}
	
	public synchronized void init(Context context) {
		if (settings == null) {
			settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
			editor = settings.edit();
			unlockedLevels = settings.getInt(UNLOCKED_LEVEL_KEY, 1);
			soundEnabled = settings.getBoolean(SOUND_KEY, true);
			musicEnabled = settings.getBoolean(MUSIC_KEY, true);
		}
	}
	
	public synchronized int getMaxUnlockedLevel() {
		return unlockedLevels;
	}
	
	public synchronized void unlockNextLevel() {
		unlockedLevels++;
		editor.putInt(UNLOCKED_LEVEL_KEY, unlockedLevels);
		editor.commit();
	}
	
	public synchronized boolean isSoundEnabled() {
		return soundEnabled;
	}
	
	public synchronized void setSoundEnabled(boolean soundEnabled) {
		editor.putBoolean(SOUND_KEY, soundEnabled);
		editor.commit();
	}
	
	public synchronized boolean isMusicEnabled() {
		return musicEnabled;
	}
	
	public synchronized void setMusicEnabled(boolean musicEnabled) {
		editor.putBoolean(MUSIC_KEY, musicEnabled);
		editor.commit();
	}
	
}
