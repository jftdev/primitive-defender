package fr.jftdev.jeux.primitivedefender.manager;

import java.io.IOException;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.util.color.Color;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import fr.jftdev.jeux.primitivedefender.entity.PDEntityGenericFactory;

public class ResourcesManager {

	public float cameraWidth = 0.0f;
	public float cameraHeight = 0.0f;
	
	public Engine engine;
	public Context context;
	
	public PDEntityGenericFactory factory = new PDEntityGenericFactory();
	
	public Sound sound;
	public Font	bellotaBold;
	public Font	bellotaBoldBig;
	
	private static ResourcesManager INSTANCE = new ResourcesManager();
	
	private ResourcesManager() {
		
	}
	
	/**
	 * Récupération d'une instance du ResourcesManager
	 * @return
	 */
	public static ResourcesManager getInstance() {
		return INSTANCE;
	}
	
	public void setup(final Engine pEngine, final Context pContext){
		engine = pEngine;
		context = pContext;
	}
	
	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	
	public synchronized void loadSounds(Engine pEngine, Context pContext){
		SoundFactory.setAssetBasePath("sounds/");
		 try {
			 sound	= SoundFactory.createSoundFromAsset(pEngine.getSoundManager(), pContext, "sound.mp3");			 
		 } catch (final IOException e) {
             Log.v("Sounds Load","Exception:" + e.getMessage());
		 }
	}	
	
	public synchronized void unloadSounds(){
		if(!sound.isReleased())sound.release();
	}
	
	public synchronized void loadFonts(Engine pEngine, Context pContext){
		FontFactory.setAssetBasePath("fonts/");
		
		bellotaBold = FontFactory.createFromAsset(pEngine.getFontManager(), pEngine.getTextureManager(), 512, 512, pContext.getAssets(), "bellota-bold-webfont.ttf",  32f, true, Color.WHITE_ABGR_PACKED_INT);
		bellotaBold.load();
		
		bellotaBoldBig = FontFactory.createFromAsset(pEngine.getFontManager(), pEngine.getTextureManager(), 512, 512, pContext.getAssets(), "bellota-bold-webfont.ttf",  64f, true, Color.WHITE_ABGR_PACKED_INT);
		bellotaBoldBig.load();
		
	}
	
	public synchronized void unloadFonts(){
		bellotaBold.unload();
	}

	public static void loadMenuResources() {
		
	}

}
