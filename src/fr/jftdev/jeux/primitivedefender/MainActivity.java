package fr.jftdev.jeux.primitivedefender;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.util.FPSLogger;
import org.andengine.input.touch.TouchEvent;
import org.andengine.ui.activity.BaseGameActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import fr.jftdev.jeux.primitivedefender.entity.PDEntityType;
import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;
import fr.jftdev.jeux.primitivedefender.manager.SceneManager;
import fr.jftdev.jeux.primitivedefender.scene.MainMenu;

public class MainActivity extends BaseGameActivity {
	
	private static final String TAG = "PrimitiveDefender.MainActivity";
	
	@Override
	protected void onCreate(Bundle pSavedInstanceState) {
		super.onCreate(pSavedInstanceState);
		ResourcesManager.getInstance().setEngine(getEngine());
	}

	@Override
	public EngineOptions onCreateEngineOptions() {
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		ResourcesManager.getInstance().cameraWidth = 1000.0f;
		ResourcesManager.getInstance().cameraHeight = 1000.0f *((float)size.y/(float)size.x);
		
		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(ResourcesManager.getInstance().cameraWidth, ResourcesManager.getInstance().cameraHeight), new Camera(0, 0, ResourcesManager.getInstance().cameraWidth, ResourcesManager.getInstance().cameraHeight));
		
		// Enable sounds.
		engineOptions.getAudioOptions().setNeedsSound(true);
		// Enable music.
		engineOptions.getAudioOptions().setNeedsMusic(true);
		// Turn on Dithering to smooth texture gradients.
		engineOptions.getRenderOptions().setDithering(true);
		// Turn on MultiSampling to smooth the alias of hard-edge elements.
		engineOptions.getRenderOptions().setMultiSampling(true);
		// Turn on MultiTouch
		engineOptions.getTouchOptions().setNeedsMultiTouch(true);
		// Set the Wake Lock options to prevent the engine from dumping textures when focus changes.
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		
		return engineOptions;
	}

	@Override
	public void onCreateResources(OnCreateResourcesCallback callback)
			throws Exception {
		
		ResourcesManager.getInstance().setup(this.getEngine(), this.getApplicationContext());
		ResourcesManager.getInstance().loadFonts(this.getEngine(), this.getApplicationContext());
		
		callback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback callback) throws Exception {
		//Scene scene = new Scene();
		getEngine().registerUpdateHandler(new FPSLogger());
		SceneManager.getInstance().showMainMenu();
		callback.onCreateSceneFinished(MainMenu.getInstance());
	}
	

	@Override
	public void onPopulateScene(Scene scene, OnPopulateSceneCallback callback)
			throws Exception {
		
		/*Enemy enemies[] = new Enemy[2];
		enemies[0] = new EnemyTest(0, (ResourcesManager.HALF_CAMERA_HEIGHT)-100.0f);
		enemies[1] = new EnemyTest(0, (ResourcesManager.HALF_CAMERA_HEIGHT)+100.0f);
		
		
		scene.attachChild(enemies[0]);
		scene.attachChild(enemies[1]);
		final Emitter weaponEmitter = new Emitter(enemies);
		scene.attachChild(weaponEmitter);*/
		
	
		
		/*scene.attachChild(ResourcesManager.getInstance().factory.create(0.0f, 500.0f, PDEntityType.RECTANGLE_NOIR));

		scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			
			@Override
			public boolean onSceneTouchEvent(Scene scene, TouchEvent event) {
				if(event.isActionDown()) {
					if(scene.getChildCount()%2 == 0) {
						scene.attachChild(ResourcesManager.getInstance().factory.create(0.0f, event.getY(), PDEntityType.RECTANGLE_NOIR));
					}
					else {
						scene.attachChild(ResourcesManager.getInstance().factory.create(0.0f, event.getY(), PDEntityType.RECTANGLE_VERT));
					}
					return true;
				}
				return false;
			}
		});
		*/
		callback.onPopulateSceneFinished();
	}
	
	/*private class Emitter extends Entity {
		final static int NUMBER_OF_WEAPONS = 1;
		final static int WEAPON_1 = 0;
		private IWeapon weapons[];
		
		public Emitter(Enemy[] enemies) {
			super();
			weapons = new IWeapon[NUMBER_OF_WEAPONS];
			weapons[WEAPON_1] = new MainWeapon(WEAPON_SELECTOR_WIDTH, ResourcesManager.HALF_CAMERA_HEIGHT);
			weapons[WEAPON_1].setEnemies(enemies);
			this.attachChild(weapons[WEAPON_1]);
		}

		public void fire(float x, float y) {
			weapons[WEAPON_1].fire(x, y);
		}
		
		public void stop() {
			weapons[WEAPON_1].stop();
		}
	}*/

}
