package fr.jftdev.jeux.primitivedefender.game;

import org.andengine.util.adt.list.SmartList;

public abstract class GameEventsDAO {

	public GameEventsDAO() {
	}
	
	public abstract SmartList<GameEvent> getSmartList(int level);

}
