package fr.jftdev.jeux.primitivedefender.game;

import fr.jftdev.jeux.primitivedefender.entity.PDEntityType;

public class GameEvent {

	private float appearTime, x, y;
	private PDEntityType type;
	
	public GameEvent() {}
	
	public GameEvent(float appearTime, float x, float y, PDEntityType type) {
		super();
		this.appearTime = appearTime;
		this.x = x;
		this.y = y;
		this.type = type;
	}

	public float getAppearTime() {
		return appearTime;
	}

	public void setAppearTime(float appearTime) {
		this.appearTime = appearTime;
	}

	public PDEntityType getType() {
		return type;
	}

	public void setType(PDEntityType type) {
		this.type = type;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

}
