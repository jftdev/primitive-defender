package fr.jftdev.jeux.primitivedefender.game;

import org.andengine.util.adt.list.SmartList;

import fr.jftdev.jeux.primitivedefender.entity.PDEntityType;
import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;

public class GameEventsDAOTest extends GameEventsDAO {

	public GameEventsDAOTest() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public SmartList<GameEvent> getSmartList(int level) {
		SmartList<GameEvent> list = new SmartList<GameEvent>();
		
		list.add(new GameEvent(1.0f, 0.0f, ResourcesManager.getInstance().cameraHeight / 2, PDEntityType.RECTANGLE_NOIR));
		
		list.add(new GameEvent(3.0f, 0.0f, 1*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_NOIR));
		list.add(new GameEvent(3.0f, 0.0f, 2*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_VERT));
		list.add(new GameEvent(3.0f, 0.0f, 3*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_NOIR));
		list.add(new GameEvent(3.0f, 0.0f, 4*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_VERT));
		list.add(new GameEvent(3.0f, 0.0f, 5*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_NOIR));
		
		list.add(new GameEvent(6.0f, 0.0f, 1*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_NOIR));
		list.add(new GameEvent(6.0f, 0.0f, 2*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_VERT));
		list.add(new GameEvent(6.0f, 0.0f, 3*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_NOIR));
		list.add(new GameEvent(6.0f, 0.0f, 4*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_VERT));
		list.add(new GameEvent(6.0f, 0.0f, 5*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_NOIR));
		
		list.add(new GameEvent(9.0f, 0.0f, 1*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_NOIR));
		list.add(new GameEvent(9.0f, 0.0f, 2*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_VERT));
		list.add(new GameEvent(9.0f, 0.0f, 3*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_NOIR));
		list.add(new GameEvent(9.0f, 0.0f, 4*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_VERT));
		list.add(new GameEvent(9.0f, 0.0f, 5*ResourcesManager.getInstance().cameraHeight / 6, PDEntityType.RECTANGLE_NOIR));
		
		return list;
	}

}
