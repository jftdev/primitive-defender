package fr.jftdev.jeux.primitivedefender.game;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.util.IMatcher;
import org.andengine.util.adt.list.SmartList;
import org.andengine.util.call.ParameterCallable;

import fr.jftdev.jeux.primitivedefender.manager.PoolManager;

public class GameEvents implements IUpdateHandler {

	private SmartList<GameEvent> gameEvents;
	private float totalElapsedTime = 0.0f;
	private Entity scene;
	
	public GameEvents(Entity enemiesLayer) {
		this.scene = enemiesLayer;
		enemiesLayer.registerUpdateHandler(this);
	}

	public SmartList<GameEvent> getGameEventsList() {
		return gameEvents;
	}

	public void setGameEventsList(SmartList<GameEvent> gameEvents) {
		this.gameEvents = gameEvents;
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {
		//TODO Pourra être optimisé
		totalElapsedTime += pSecondsElapsed;
		this.gameEvents.removeAll(new IMatcher<GameEvent>() {
			
			@Override
			public boolean matches(GameEvent pObject) {
				return pObject.getAppearTime() < totalElapsedTime;
			}
		}, new ParameterCallable<GameEvent>() {
			
			@Override
			public void call(GameEvent pParameter) {
				scene.attachChild(PoolManager.getInstance().getPool(pParameter.getType()).obtainPoolItem(pParameter.getX(), pParameter.getY()));
			}
		});
	}

	@Override
	public void reset() {
		totalElapsedTime = 0.0f;
	}

}
