package fr.jftdev.jeux.primitivedefender.game;

import org.andengine.entity.Entity;

public class GameEventsFactory {

	public GameEventsFactory() {
	}
	
	public static GameEvents create(int level, Entity enemiesLayer) {
		GameEvents gameEvents = new GameEvents(enemiesLayer);
		GameEventsDAO dao = new GameEventsDAOTest();
		gameEvents.setGameEventsList(dao.getSmartList(level));
		return gameEvents;
	}

	
}
