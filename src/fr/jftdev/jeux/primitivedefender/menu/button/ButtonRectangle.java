package fr.jftdev.jeux.primitivedefender.menu.button;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class ButtonRectangle extends Rectangle {

	private OnClickListener mOnClickListener;

	private boolean mEnabled = true;
	private State mState;

	// ===========================================================
	// Constructors
	// ===========================================================
	public ButtonRectangle(final float pX, final float pY, final float pWidth, final float pHeight, final VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pWidth, pHeight, pVertexBufferObjectManager);
		this.changeState(State.NORMAL);
	}
	
	public ButtonRectangle(final float pX, final float pY, final float pWidth, final float pHeight, final VertexBufferObjectManager pVertexBufferObjectManager, final OnClickListener pOnClickListener) {
		super(pX, pY, pWidth, pHeight, pVertexBufferObjectManager);
		this.mOnClickListener = pOnClickListener;
		this.changeState(State.NORMAL);
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	public boolean isEnabled() {
		return this.mEnabled;
	}

	public void setEnabled(final boolean pEnabled) {
		this.mEnabled = pEnabled;

		if(this.mEnabled && this.mState == State.DISABLED) {
			this.changeState(State.NORMAL);
		} else if(!this.mEnabled) {
			this.changeState(State.DISABLED);
		}
	}

	public boolean isPressed() {
		return this.mState == State.PRESSED;
	}

	public State getState() {
		return this.mState;
	}

	public void setOnClickListener(final OnClickListener pOnClickListener) {
		this.mOnClickListener = pOnClickListener;
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
		
		if(this.mOnClickListener != null) {
			this.mOnClickListener.onClick(this, pTouchAreaLocalX, pTouchAreaLocalY);
			return true;
		}
		return false;
		
		/*if(!this.isEnabled()) {
			this.changeState(State.DISABLED);
		} else if(pSceneTouchEvent.isActionDown()) {
			this.changeState(State.PRESSED);
		} else if(pSceneTouchEvent.isActionCancel() || !this.contains(pSceneTouchEvent.getX(), pSceneTouchEvent.getY())) {
			this.changeState(State.NORMAL);
		} else if(pSceneTouchEvent.isActionUp() && this.mState == State.PRESSED) {
			this.changeState(State.NORMAL);

		}*/

	}

	@Override
	public boolean contains(final float pX, final float pY) {
		if(!this.isVisible()) {
			return false;
		} else {
			return super.contains(pX, pY);
		}
	}

	// ===========================================================
	// Methods
	// ===========================================================

	private void changeState(final State pState) {
		return;
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	public interface OnClickListener {
		// ===========================================================
		// Constants
		// ===========================================================

		// ===========================================================
		// Methods
		// ===========================================================

		public void onClick(final ButtonRectangle pButtonRectangle, final float pTouchAreaLocalX, final float pTouchAreaLocalY);
	}

	public static enum State {
		NORMAL,
		PRESSED,
		DISABLED
	}
}
