package fr.jftdev.jeux.primitivedefender.scene;

import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.ScaleAtModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.util.modifier.ease.EaseBounceOut;

import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;
import fr.jftdev.jeux.primitivedefender.manager.SceneManager;
import fr.jftdev.jeux.primitivedefender.menu.button.ButtonRectangle;
import fr.jftdev.jeux.primitivedefender.menu.button.ButtonRectangle.OnClickListener;

public class MainMenu extends ManagedMenuScene {
	
	private static final MainMenu INSTANCE = new MainMenu();
	public static MainMenu getInstance(){
		return INSTANCE;
	}
	
	public MainMenu() {
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
	}
	
	// No loading screen means no reason to use the following methods.
	@Override
	public Scene onLoadingScreenLoadAndShown() {
		return null;
	}
	@Override
	public void onLoadingScreenUnloadAndHidden() {
	}
	
	// The objects that will make up our Main Menu
	private ButtonRectangle PlayButton;
	private Text ApplicationTitle;
	@Override
	public void onLoadScene() {
		// Load the menu resources
		ResourcesManager.loadMenuResources();
		
		this.setBackground(new Background(1.0f, 1.0f, 1.0f));
		
		PlayButton = new ButtonRectangle((ResourcesManager.getInstance().cameraWidth/2.0f)-100.0f, -80.0f, 200.0f, 80.0f, ResourcesManager.getInstance().engine.getVertexBufferObjectManager());
		PlayButton.setColor(0.0f, 0.0f, 0.0f);
		Text playText = new Text(60.0f, 14.0f, ResourcesManager.getInstance().bellotaBold, "PLAY", ResourcesManager.getInstance().engine.getVertexBufferObjectManager());
		playText.setColor(1.0f, 1.0f, 1.0f);
		PlayButton.attachChild(playText);
		this.attachChild(PlayButton);
		MoveYModifier moveModifier = new MoveYModifier(1.0f, -80.0f, ResourcesManager.getInstance().cameraHeight/2+100.0f, EaseBounceOut.getInstance());
		DelayModifier delayModifier = new DelayModifier(0.3f);
		SequenceEntityModifier sequenceModifier = new SequenceEntityModifier(delayModifier, moveModifier);
		PlayButton.registerEntityModifier(sequenceModifier);
		
		ApplicationTitle = new Text(0.0f, 120.0f, ResourcesManager.getInstance().bellotaBoldBig, "Primitive Defender", ResourcesManager.getInstance().engine.getVertexBufferObjectManager());
		ApplicationTitle.setColor(0.0f, 0.0f, 0.0f);
		ApplicationTitle.setX((ResourcesManager.getInstance().cameraWidth - ApplicationTitle.getWidth()) / 2);
		ApplicationTitle.setScale(0.0f);
		this.attachChild(ApplicationTitle);
		
		ScaleAtModifier scaleModifier = new ScaleAtModifier(1.0f, 0.0f, 1.0f, ApplicationTitle.getWidth()/2, ApplicationTitle.getHeight()/2, EaseBounceOut.getInstance());
		DelayModifier delayAppModifier = new DelayModifier(0.8f);
		SequenceEntityModifier sequenceAppModifier = new SequenceEntityModifier(delayAppModifier, scaleModifier);
		ApplicationTitle.registerEntityModifier(sequenceAppModifier);
		
		
		
		PlayButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(ButtonRectangle pButtonSprite,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				// Create a new GameLevel and show it using the SceneManager. And play a click.
				SceneManager.getInstance().showScene(new GameLevel());
				//ResourcesManager.clickSound.play();
			}});
		
		this.registerTouchArea(PlayButton);
		
	}
	
	@Override
	public void onShowScene() {
	}
	@Override
	public void onHideScene() {
	}
	@Override
	public void onUnloadScene() {
	}
}