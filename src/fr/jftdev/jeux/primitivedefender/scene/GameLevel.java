package fr.jftdev.jeux.primitivedefender.scene;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.input.touch.TouchEvent;

import android.util.Log;
import fr.jftdev.jeux.primitivedefender.entity.WeaponSelector;
import fr.jftdev.jeux.primitivedefender.entity.WeaponSelector.OnWeaponChange;
import fr.jftdev.jeux.primitivedefender.game.GameEvents;
import fr.jftdev.jeux.primitivedefender.game.GameEventsFactory;
import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;
import fr.jftdev.jeux.primitivedefender.manager.WeaponManager;
import fr.jftdev.jeux.primitivedefender.util.constant.WeaponConstants;


public class GameLevel extends ManagedScene implements OnWeaponChange, IOnSceneTouchListener {

	private int currentWeapon = 0;
	private WeaponSelector weapon1Selector;
	private WeaponSelector weapon2Selector;
	private WeaponSelector weapon3Selector;
	private WeaponSelector weapon4Selector;
	private WeaponSelector weapon5Selector;
	private WeaponManager weaponManager;
	private GameEvents gameEvents;
	private Entity enemiesLayer;
	
	public GameLevel() {
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
		this.setOnSceneTouchListener(this);
	}

	@Override
	public Scene onLoadingScreenLoadAndShown() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLoadScene() {
		this.setBackground(new Background(1.0f, 1.0f, 1.0f));
		this.enemiesLayer = new Entity();
		this.loadWeaponSelector();
		this.loadBattleField();
		this.gameEvents = GameEventsFactory.create(1, this.enemiesLayer);
		this.attachChild(enemiesLayer);
	}

	@Override
	public void onShowScene() {

	}

	@Override
	public void onHideScene() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUnloadScene() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		super.onManagedUpdate(pSecondsElapsed);
	}
	
	private void loadWeaponSelector() {
		
		weapon1Selector = new WeaponSelector(0.0f, 0.0f * ResourcesManager.getInstance().cameraHeight/5, WeaponConstants.WEAPON_SELECTOR_WIDTH, ResourcesManager.getInstance().cameraHeight/5, ResourcesManager.getInstance().engine.getVertexBufferObjectManager(), 0, true);
		weapon2Selector = new WeaponSelector(0.0f, 1.0f * ResourcesManager.getInstance().cameraHeight/5, WeaponConstants.WEAPON_SELECTOR_WIDTH, ResourcesManager.getInstance().cameraHeight/5, ResourcesManager.getInstance().engine.getVertexBufferObjectManager(), 1, true);
		weapon3Selector = new WeaponSelector(0.0f, 2.0f * ResourcesManager.getInstance().cameraHeight/5, WeaponConstants.WEAPON_SELECTOR_WIDTH, ResourcesManager.getInstance().cameraHeight/5, ResourcesManager.getInstance().engine.getVertexBufferObjectManager(), 2, true);
		weapon4Selector = new WeaponSelector(0.0f, 3.0f * ResourcesManager.getInstance().cameraHeight/5, WeaponConstants.WEAPON_SELECTOR_WIDTH, ResourcesManager.getInstance().cameraHeight/5, ResourcesManager.getInstance().engine.getVertexBufferObjectManager(), 3, true);
		weapon5Selector = new WeaponSelector(0.0f, 4.0f * ResourcesManager.getInstance().cameraHeight/5, WeaponConstants.WEAPON_SELECTOR_WIDTH, ResourcesManager.getInstance().cameraHeight/5, ResourcesManager.getInstance().engine.getVertexBufferObjectManager(), 4, true);
		
		weapon1Selector.setWeaponChangeListener(this);
		weapon2Selector.setWeaponChangeListener(this);
		weapon3Selector.setWeaponChangeListener(this);
		weapon4Selector.setWeaponChangeListener(this);
		weapon5Selector.setWeaponChangeListener(this);
		
		weapon1Selector.setColor(1.0f, 0.5f, 0.0f);
		weapon2Selector.setColor(0.0f, 1.0f, 0.0f);
		weapon3Selector.setColor(1.0f, 1.0f, 0.0f);
		weapon4Selector.setColor(0.0f, 0.5f, 0.5f);
		weapon5Selector.setColor(1.0f, 1.0f, 0.5f);
		
		this.attachChild(weapon1Selector);
		this.attachChild(weapon2Selector);
		this.attachChild(weapon3Selector);
		this.attachChild(weapon4Selector);
		this.attachChild(weapon5Selector);
		
		this.registerTouchArea(weapon1Selector);
		this.registerTouchArea(weapon2Selector);
		this.registerTouchArea(weapon3Selector);
		this.registerTouchArea(weapon4Selector);
		this.registerTouchArea(weapon5Selector);
	}
	
	private void loadBattleField() {
		weaponManager = new WeaponManager(this, enemiesLayer);
	}

	@Override
	public void weaponChange(int weaponId) {
		Log.v("PD", "Weapon selected " + weaponId);
		this.currentWeapon = weaponId;
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		if(pSceneTouchEvent.getX() > WeaponConstants.WEAPON_SELECTOR_WIDTH) {
			if(pSceneTouchEvent.isActionCancel() || pSceneTouchEvent.isActionUp()  || pSceneTouchEvent.isActionOutside()) {
				weaponManager.stop(currentWeapon);
				return true;
			}
			else if(pSceneTouchEvent.isActionMove()) {
				weaponManager.fire(pSceneTouchEvent.getX(), pSceneTouchEvent.getY(), currentWeapon);
				return true;
			}
		}
		return false;
	}

}