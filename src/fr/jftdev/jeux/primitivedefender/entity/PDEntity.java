package fr.jftdev.jeux.primitivedefender.entity;

import org.andengine.entity.Entity;

public abstract class PDEntity extends Entity {

	public PDEntity() {
		super();
	}

	public PDEntity(float pX, float pY) {
		super(pX, pY);
	}
	
}
