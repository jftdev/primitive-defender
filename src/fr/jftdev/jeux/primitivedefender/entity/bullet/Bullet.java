package fr.jftdev.jeux.primitivedefender.entity.bullet;

import org.andengine.entity.shape.IShape;

import fr.jftdev.jeux.primitivedefender.entity.PDEntity;

public abstract class Bullet extends PDEntity {

	protected float strength;
	
	public Bullet() {
		super();
	}

	public Bullet(float pX, float pY) {
		super(pX, pY);
	}
	
	public abstract void onShot(PDEntity target);
	public abstract IShape getHitBox();
	
}
