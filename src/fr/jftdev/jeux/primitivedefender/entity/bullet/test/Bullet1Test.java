package fr.jftdev.jeux.primitivedefender.entity.bullet.test;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.shape.IShape;

import fr.jftdev.jeux.primitivedefender.entity.PDEntity;
import fr.jftdev.jeux.primitivedefender.entity.bullet.Bullet;
import fr.jftdev.jeux.primitivedefender.entity.enemy.Enemy;
import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;

public class Bullet1Test extends Bullet {

	private Rectangle mainShape;
	
	public Bullet1Test() {
		super();
		init();
	}

	public Bullet1Test(float pX, float pY) {
		super(pX, pY);
		init();
	}
	
	private void init() {
		this.strength = 4.0f;
		mainShape = new Rectangle(0.0f, 0.0f, 5.0f, 20.0f, ResourcesManager.getInstance().engine.getVertexBufferObjectManager());
		mainShape.setColor(0.0f, 0.0f, 0.0f);
		this.attachChild(mainShape);
	}

	@Override
	public void onShot(PDEntity target) {
		if(target instanceof Enemy) {
			((Enemy)target).onShot(this.strength, null);
		}
	}

	@Override
	public void reset() {
		super.reset();
		mainShape.reset();
		mainShape.setAlpha(1.0f);
	}

	@Override
	public IShape getHitBox() {
		return mainShape;
	}

}
