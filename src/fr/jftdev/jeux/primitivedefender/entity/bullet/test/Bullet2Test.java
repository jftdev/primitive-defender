package fr.jftdev.jeux.primitivedefender.entity.bullet.test;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.shape.IShape;

import fr.jftdev.jeux.primitivedefender.entity.PDEntity;
import fr.jftdev.jeux.primitivedefender.entity.bullet.Bullet;
import fr.jftdev.jeux.primitivedefender.entity.enemy.Enemy;
import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;

public class Bullet2Test extends Bullet {

	private Rectangle mainShape;
	
	public Bullet2Test() {
		super();
		init();
	}

	public Bullet2Test(float pX, float pY) {
		super(pX, pY);
		init();
	}
	
	private void init() {
		this.strength = 0.3f;
		mainShape = new Rectangle(0.0f, (-1 * (ResourcesManager.getInstance().cameraHeight / 2)) + 30.0f, 10.0f, ResourcesManager.getInstance().cameraHeight - 60.0f, ResourcesManager.getInstance().engine.getVertexBufferObjectManager());
		mainShape.setColor(0.0f, 0.6f, 0.0f);
		this.attachChild(mainShape);
	}

	@Override
	public void onShot(PDEntity target) {
		if(target instanceof Enemy) {
			((Enemy)target).onShot(this.strength * this.mainShape.getAlpha(), null);
		}
	}

	@Override
	public void setAlpha(float pAlpha) {
		super.setAlpha(pAlpha);
		mainShape.setAlpha(pAlpha);
	}

	@Override
	public void reset() {
		super.reset();
		mainShape.setAlpha(1.0f);
		mainShape.setColor(0.0f, 0.6f, 0.0f);
	}

	@Override
	public IShape getHitBox() {
		return mainShape;
	}

}
