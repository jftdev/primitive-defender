package fr.jftdev.jeux.primitivedefender.entity;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.util.Log;

public class WeaponSelector extends Rectangle {
	
	OnWeaponChange listener;
	private int weaponId;
	private boolean active;

	public WeaponSelector(float pX, float pY, float pWidth, float pHeight,
			VertexBufferObjectManager pVertexBufferObjectManager, int weaponId, boolean isActive) {
		super(pX, pY, pWidth, pHeight, pVertexBufferObjectManager);
		this.weaponId = weaponId;
		this.setActive(isActive);
	}
	
	public void setWeaponChangeListener(OnWeaponChange listener) {
		this.listener = listener;
	}

	public interface OnWeaponChange {
		public void weaponChange(int weaponId);
	}
	
	@Override
	public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
		
		float[] scenePos = this.convertLocalToSceneCoordinates(pTouchAreaLocalX, pTouchAreaLocalY);
		boolean containsScene = this.contains(scenePos[0], scenePos[1]);
		
		if(listener != null && containsScene) {
			listener.weaponChange(weaponId);
			return true;
		}
		return false;

	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
