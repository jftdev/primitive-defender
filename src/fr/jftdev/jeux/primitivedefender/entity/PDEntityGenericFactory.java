package fr.jftdev.jeux.primitivedefender.entity;

import fr.jftdev.jeux.primitivedefender.manager.PoolManager;

public class PDEntityGenericFactory {

	public PDEntityGenericFactory() {
	}
	
	public PDEntity create(float pX, float pY, String type) {
		return this.create(pX, pY, PDEntityType.valueOf(type));
	}

	public PDEntity create(float pX, float pY, PDEntityType type) {
		return PoolManager.getInstance().getPool(type).obtainPoolItem(pX, pY);
	}

}
