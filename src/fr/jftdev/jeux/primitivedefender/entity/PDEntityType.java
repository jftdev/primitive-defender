package fr.jftdev.jeux.primitivedefender.entity;

public enum PDEntityType {
	
	/**************************************
	 * Enemies de test
	 **************************************/
	RECTANGLE_NOIR,
	RECTANGLE_VERT,
	
	/**************************************
	 * Balles de test
	 **************************************/
	BULLET_1_TEST,
	BULLET_2_TEST,
	
	/**************************************
	 * Enemies de test
	 **************************************/
	EXPLOSION_1_TEST,
	EXPLOSION_2_TEST
}
