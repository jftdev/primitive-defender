package fr.jftdev.jeux.primitivedefender.entity;

import org.andengine.util.adt.pool.GenericPool;

public class PDEntityPool<T extends PDEntity> {

	GenericPool<T> pool;
	
	public PDEntityPool(final Class<T> classType, int size) {
		this.pool = new GenericPool<T>(size) {
			@Override
			protected T onAllocatePoolItem() {
				try {
					return classType.newInstance();
				} catch (InstantiationException e) {
					return null;
				} catch (IllegalAccessException e) {
					return null;
				}
			}
		};
	}

	public synchronized T obtainPoolItem(final float pX, final float pY) {
		T entity = this.pool.obtainPoolItem();
		entity.setPosition(pX, pY);
		entity.reset();
		return entity;
	}
	
	public synchronized <U extends PDEntity> void recyclePoolItem(U pItem) {
		this.pool.recyclePoolItem((T)pItem);
	}

}
