package fr.jftdev.jeux.primitivedefender.entity;

import org.andengine.entity.IEntityFactory;

import fr.jftdev.jeux.primitivedefender.manager.PoolManager;

public class PDEntityParticleFactory<T extends PDEntity> implements IEntityFactory<T> {

	protected PDEntityType type;
	
	public PDEntityParticleFactory(String typeString) {
		super();
		this.type = PDEntityType.valueOf(typeString);
	}

	public PDEntityParticleFactory(PDEntityType type) {
		super();
		this.type = type;
	}

	@Override
	public T create(float pX, float pY) {
		return (T) PoolManager.getInstance().getPool(type).obtainPoolItem(pX, pY);
	}

}
