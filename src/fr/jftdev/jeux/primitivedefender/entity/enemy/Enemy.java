package fr.jftdev.jeux.primitivedefender.entity.enemy;

import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.shape.IShape;

import fr.jftdev.jeux.primitivedefender.entity.PDEntity;
import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;

public abstract class Enemy extends PDEntity {

	protected float life;
	
	public Enemy() {
		super();
	}

	public Enemy(float pX, float pY) {
		super(pX, pY);
	}
	
	public synchronized void onShot(float damage, final IEntityModifier modifier) {
		life -= damage;
		if(life <= 0.0f) {
			ResourcesManager.getInstance().engine.runOnUpdateThread(new Runnable() {
				
				@Override
				public void run() {
					onDie();
				}
			}, true);
		}
	}
	
	protected synchronized void incrementLife(float value) {
		life += value;
	}
	
	protected synchronized void decrementLife(float value) {
		life -= value;
	}
	
	public abstract void onDie();
	
	public abstract IShape getHitBox();
	
	public boolean isAlive() {
		return life > 0.0f;
	}
	
}
