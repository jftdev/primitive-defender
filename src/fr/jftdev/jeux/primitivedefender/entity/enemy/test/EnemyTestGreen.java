package fr.jftdev.jeux.primitivedefender.entity.enemy.test;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveByModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.shape.IShape;

import fr.jftdev.jeux.primitivedefender.entity.PDEntityType;
import fr.jftdev.jeux.primitivedefender.entity.enemy.Enemy;
import fr.jftdev.jeux.primitivedefender.manager.PoolManager;
import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;
import fr.jftdev.jeux.primitivedefender.util.constant.WeaponConstants;

public class EnemyTestGreen extends Enemy {

	private Rectangle mainShape;
	private final PDEntityType type = PDEntityType.RECTANGLE_VERT;
	
	public EnemyTestGreen() {
		super();
		init();
	}

	public EnemyTestGreen(float pX, float pY) {
		super(pX, pY);
		init();
	}
	
	private void init() {
		mainShape = new Rectangle(0.0f, 0.0f, 50.0f, 50.0f, ResourcesManager.getInstance().engine.getVertexBufferObjectManager());
		this.attachChild(mainShape);
	}

	@Override
	public void reset() {
		super.reset();
		this.life = 10.0f;
		mainShape.reset();
		mainShape.setColor(0.0f, 1.0f, 0.0f);
		mainShape.setPosition(ResourcesManager.getInstance().cameraWidth + 50.f, 0.0f);
		mainShape.clearUpdateHandlers();
		mainShape.registerEntityModifier(new LoopEntityModifier(new MoveByModifier(1.0f, -100.0f, 0.0f)));
		mainShape.registerUpdateHandler(new IUpdateHandler() {
			
			@Override
			public void reset() {
				
			}
			
			@Override
			public void onUpdate(float pSecondsElapsed) {
				if(mainShape.getX() <= WeaponConstants.WEAPON_SELECTOR_WIDTH) {
					mainShape.clearEntityModifiers();
				}
			}
		});
		this.setVisible(true);
	}

	@Override
	public void onDie() {
		this.setVisible(false);
		this.getParent().detachChild(this);
		PoolManager.getInstance().getPool(this.type).recyclePoolItem(this);
	}

	@Override
	public IShape getHitBox() {
		return mainShape;
	}

}
