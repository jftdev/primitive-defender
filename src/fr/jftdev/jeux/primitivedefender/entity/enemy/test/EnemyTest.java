package fr.jftdev.jeux.primitivedefender.entity.enemy.test;

import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.shape.IShape;

import fr.jftdev.jeux.primitivedefender.entity.PDEntityType;
import fr.jftdev.jeux.primitivedefender.entity.enemy.Enemy;
import fr.jftdev.jeux.primitivedefender.manager.PoolManager;
import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;
import fr.jftdev.jeux.primitivedefender.util.constant.WeaponConstants;

public class EnemyTest extends Enemy {

	private Rectangle mainShape;
	private MoveXModifier moveXModifier;
	private PDEntityType type = PDEntityType.RECTANGLE_NOIR;
	
	public EnemyTest() {
		super();
		init();
	}

	public EnemyTest(float pX, float pY) {
		super(pX, pY);
		init();
	}
	
	private void init() {
		mainShape = new Rectangle(this.mX, this.mY, 50.0f, 50.0f, ResourcesManager.getInstance().engine.getVertexBufferObjectManager());
		mainShape.setColor(0.0f, 0.0f, 0.0f);
		moveXModifier = new MoveXModifier(10, ResourcesManager.getInstance().cameraWidth , WeaponConstants.WEAPON_SELECTOR_WIDTH);
		mainShape.registerEntityModifier(moveXModifier);
		this.attachChild(mainShape);
	}

	@Override
	public void reset() {
		this.life = 15.0f;
		mainShape.reset();
		this.setVisible(true);
	}

	@Override
	public void onDie() {
		this.setVisible(false);
		this.getParent().detachChild(this);
		PoolManager.getInstance().getPool(this.type).recyclePoolItem(this);
	}

	@Override
	public IShape getHitBox() {
		return mainShape;
	}

}
