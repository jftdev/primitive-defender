package fr.jftdev.jeux.primitivedefender.entity.weapon;

import org.andengine.entity.Entity;

public abstract class Weapon extends Entity {
	
	public Weapon() {
		super(0.0f,0.0f);
	}
	public Weapon(float pX, float pY) {
		super(pX, pY);
	}
	public abstract void fire(float x, float y);
	public abstract void stop();
	public abstract float getRate();
}
