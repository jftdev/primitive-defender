package fr.jftdev.jeux.primitivedefender.entity.weapon.test;

import org.andengine.entity.Entity;
import org.andengine.entity.particle.Particle;
import org.andengine.entity.particle.ParticleSystem;
import org.andengine.entity.particle.emitter.PointParticleEmitter;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.AlphaParticleModifier;
import org.andengine.entity.particle.modifier.ExpireParticleInitializer;
import org.andengine.entity.particle.modifier.IParticleModifier;

import fr.jftdev.jeux.primitivedefender.entity.PDEntityParticleFactory;
import fr.jftdev.jeux.primitivedefender.entity.PDEntityType;
import fr.jftdev.jeux.primitivedefender.entity.bullet.test.Bullet2Test;
import fr.jftdev.jeux.primitivedefender.entity.enemy.Enemy;
import fr.jftdev.jeux.primitivedefender.entity.weapon.Weapon;
import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;
import fr.jftdev.jeux.primitivedefender.util.constant.WeaponConstants;

public class Weapon2Test extends Weapon {

	private ParticleSystem<Bullet2Test> particleSystem;
	private VelocityParticleInitializer<Bullet2Test> velocityParticleInitializer;
	private Entity enemiesLayer;
	
	private static final float BASE_RATE = 0.4f;
	private static final int BASE_MAX = 30;
	private static final float BASE_SPEED = 200f;
	
	public Weapon2Test() {
		super();
		init();
	}
	
	public Weapon2Test(float pX, float pY, Entity enemiesLayer) {
		super(pX, pY);
		this.enemiesLayer = enemiesLayer;
		init();
	}
	
	private void init() {
		PointParticleEmitter particleEmmitter = new PointParticleEmitter(WeaponConstants.WEAPON_SELECTOR_WIDTH, ResourcesManager.getInstance().cameraHeight / 2);
		particleSystem = new ParticleSystem<Bullet2Test>(new PDEntityParticleFactory<Bullet2Test>(PDEntityType.BULLET_2_TEST), particleEmmitter, BASE_RATE, BASE_RATE, BASE_MAX);
		
		particleSystem.setParticlesSpawnEnabled(false);
		
		velocityParticleInitializer = new VelocityParticleInitializer<Bullet2Test>(BASE_SPEED, 0.0f);
		particleSystem.addParticleInitializer(velocityParticleInitializer);
		
		particleSystem.addParticleModifier(new AlphaParticleModifier<Bullet2Test>(0.0f, 2.0f, 1.0f, 0.0f));
		
		particleSystem.addParticleInitializer(new ExpireParticleInitializer<Bullet2Test>(2.0f));
		
		particleSystem.addParticleModifier(new IParticleModifier<Bullet2Test>() {
			
			@Override
			public void onInitializeParticle(Particle<Bullet2Test> pParticle) {
			}
			
			@Override
			public void onUpdateParticle(Particle<Bullet2Test> pParticle) {
				for(int i=0; i<enemiesLayer.getChildCount(); i++) {
					final Enemy enemy = (Enemy)(enemiesLayer.getChildByIndex(i));
					if(enemy.isAlive() && pParticle.getEntity().getHitBox().collidesWith(enemy.getHitBox())) {
						pParticle.getEntity().onShot((Enemy)(enemiesLayer.getChildByIndex(i)));
						//pParticle.setExpired(true);
					}
				}
			}
		});
		
		this.attachChild(particleSystem);
	}

	@Override
	public void fire(float x, float y) {
		particleSystem.setParticlesSpawnEnabled(true);
	}

	@Override
	public void stop() {
		particleSystem.setParticlesSpawnEnabled(false);
	}
	
	@Override
	public float getRate() {
		return BASE_RATE;
	}

}
