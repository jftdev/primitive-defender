package fr.jftdev.jeux.primitivedefender.entity.weapon.test;

import org.andengine.entity.Entity;
import org.andengine.entity.particle.Particle;
import org.andengine.entity.particle.ParticleSystem;
import org.andengine.entity.particle.emitter.PointParticleEmitter;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.ExpireParticleInitializer;
import org.andengine.entity.particle.modifier.IParticleModifier;
import org.andengine.util.math.MathUtils;

import fr.jftdev.jeux.primitivedefender.entity.PDEntityParticleFactory;
import fr.jftdev.jeux.primitivedefender.entity.PDEntityType;
import fr.jftdev.jeux.primitivedefender.entity.bullet.test.Bullet1Test;
import fr.jftdev.jeux.primitivedefender.entity.enemy.Enemy;
import fr.jftdev.jeux.primitivedefender.entity.weapon.Weapon;
import fr.jftdev.jeux.primitivedefender.manager.ResourcesManager;
import fr.jftdev.jeux.primitivedefender.util.constant.WeaponConstants;

public class Weapon1Test extends Weapon {

	private ParticleSystem<Bullet1Test> particleSystem;
	private VelocityParticleInitializer<Bullet1Test> velocityParticleInitializer;
	private RotationParticleInitializer<Bullet1Test> rotationParticleInitializer;
	
	private static final float BASE_RATE = 2.0f;
	private static final int BASE_MAX = 20;
	private static final float BASE_SPEED = 400f;
	
	private Entity enemiesLayer;
	
	public Weapon1Test() {
		super();
		init();
	}
	
	public Weapon1Test(float pX, float pY, Entity enemiesLayer) {
		super(pX, pY);
		this.enemiesLayer = enemiesLayer;
		init();
	}
	
	private void init() {
		PointParticleEmitter particleEmmitter = new PointParticleEmitter(WeaponConstants.WEAPON_SELECTOR_WIDTH, ResourcesManager.getInstance().cameraHeight / 2);
		particleSystem = new ParticleSystem<Bullet1Test>(new PDEntityParticleFactory<Bullet1Test>(PDEntityType.BULLET_1_TEST), particleEmmitter, BASE_RATE, BASE_RATE, BASE_MAX);
		
		particleSystem.setParticlesSpawnEnabled(false);
		
		velocityParticleInitializer = new VelocityParticleInitializer<Bullet1Test>(200.0f);
		particleSystem.addParticleInitializer(velocityParticleInitializer);
		
		rotationParticleInitializer = new RotationParticleInitializer<Bullet1Test>(0);
		particleSystem.addParticleInitializer(rotationParticleInitializer);
		
		particleSystem.addParticleInitializer(new ExpireParticleInitializer<Bullet1Test>(5.0f));
		
		particleSystem.addParticleModifier(new IParticleModifier<Bullet1Test>() {
			
			@Override
			public void onInitializeParticle(Particle<Bullet1Test> pParticle) {
			}
			
			@Override
			public void onUpdateParticle(Particle<Bullet1Test> pParticle) {
				for(int i=0; i<enemiesLayer.getChildCount(); i++) {
					final Enemy enemy = (Enemy)(enemiesLayer.getChildByIndex(i));
					if(enemy.isAlive() && pParticle.getEntity().getHitBox().collidesWith(enemy.getHitBox())) {
						pParticle.getEntity().onShot((Enemy)(enemiesLayer.getChildByIndex(i)));
						pParticle.setExpired(true);
					}
				}
			}
		});
		
		this.attachChild(particleSystem);
	}

	@Override
	public void fire(float x, float y) {
		particleSystem.setParticlesSpawnEnabled(true);
		final float angle = MathUtils.atan2(y-ResourcesManager.getInstance().cameraHeight/2, x-WeaponConstants.WEAPON_SELECTOR_WIDTH);
		velocityParticleInitializer.setVelocityX((float) (BASE_SPEED*Math.cos(angle)));
		velocityParticleInitializer.setVelocityY((float) (BASE_SPEED*Math.sin(angle)));
		rotationParticleInitializer.setRotation(MathUtils.radToDeg(angle) - 90.0f);

	}

	@Override
	public void stop() {
		particleSystem.setParticlesSpawnEnabled(false);
	}
	
	@Override
	public float getRate() {
		return BASE_RATE;
	}

}
